<?php

	final class Tracer 
	{
		public static function trace($variable) 
		{
			echo '<pre>';
			var_dump($variable);
			echo '</pre>';
		}
		public static function traceAndExit($variable) 
		{
			self::trace($variable);
			exit;
		}
	}

	final class FrontController 
	{
		private $HTTPRequest;
		private $HTTPResponse;

		private function __construct() 
		{
			$this->HTTPRequest 	= HTTPRequest::getInstance();
			$this->HTTPResponse = HTTPResponse::getInstance();
		}
		private function getHTTPRequest() 
		{
			return $this->HTTPRequest;
		}
		private function getHTTPResponse() 
		{
			return $this->HTTPResponse;
		}
		private function isMainPage() 
		{
			return $this->getHTTPRequest()->getURL() === '/';
		}
		private function isAddFormPage() 
		{
			return $this->getHTTPRequest()->getURL() === '/addproduct';
		}
		private function isPOST() {
			return $this->getHTTPRequest()->isPOST();
		}
		private function isGET() {
			return $this->getHTTPRequest()->isGET();
		}
		private function issetPP($name) {
			return $this->getHTTPRequest()->getPOST()->isset($name);
		}
		private function isShowProducts() 
		{
			return $this->isMainPage() && $this->isGET();
		}
		private function isAddProductForm() 
		{
			return $this->isAddFormPage() && $this->isGET();
		}
		private function isAddProducts() 
		{
			return $this->isMainPage() && $this->isPOST() && $this->issetPP('sku');
		}
		private function isDeleteProducts() 
		{
			return $this->isMainPage() && $this->isPOST() && $this->issetPP('md');
		}
		public static function go() 
		{
			static $instance;
			if (!isset($instance)) {
				$instance = new self;
			}
			$instance->run();
		}
		private function view($name) 
		{
			$view = "{$name}View";
			$this->HTTPResponse->setContent((new $view)->render());
			$this->HTTPResponse->send();
		}
		private function action($name) 
		{
			$action = "{$name}Action";
			(new $action)->run();
		}
		private function showProducts() 
		{
			$this->view('ShowProducts');
		}
		private function showProductForm() 
		{
			$this->view('AddProduct');
		}
		private function showDefault() 
		{
			$this->view('Default');
		}
		private function addProduct() 
		{
			$this->action('AddProduct');
		}
		private function deleteProducts() 
		{
			$this->action('DeleteProducts');
		}
		private function run() 
		{
			if ($this->isShowProducts()) {
				$this->showProducts();
			} elseif ($this->isAddProductForm()) {
				$this->showProductForm();
			} elseif ($this->isAddProducts()) {
				$this->addProduct();
				$this->showProducts();
			} elseif ($this->isDeleteProducts()) {
				$this->deleteProducts();
				$this->showProducts();
			} else {
				$this->showProducts();
			}
		}
	}

	final class HTTPRequest 
	{
		private $GET;
		private $POST;
		private $SERVER;

		public static function getInstance() {
			static $instance;
			if (!isset($instance)) {
				$instance = new self;
			}
			return $instance;
		}
		private function __construct() {
			$this->GET 		= 	HTTPRequestGET::getInstance();
			$this->POST 	= 	HTTPRequestPOST::getInstance();
			$this->SERVER 	= 	HTTPRequestSERVER::getInstance();
		}
		public function getGET() 
		{
			return $this->GET;	
		}
		public function getPOST() 
		{
			return $this->POST;
		}
		public function getSERVER() 
		{
			return $this->SERVER;
		}
		public function getMETHOD() 
		{
			return $this->getSERVER()->get('REQUEST_METHOD');
		}
		public function isGET() 
		{
			return $this->getMETHOD() === 'GET';
		}
		public function isPOST() 
		{
			return $this->getMETHOD() === 'POST';
		}
		public function getURL() 
		{
			return $this->getSERVER()->get('REQUEST_URI');
		}
		public function countGET() 
		{
			return count($this->getGET());
		}
		public function countPOST() 
		{
			return count($this->getPOST());
		}
		public function issetGETParameter($name) 
		{
			return $this->getGET()->isset($name);
		}
		public function issetPOSTParameter($name) 
		{
			return $this->getPOST()->isset($name);
		}
	}

	abstract class HTTPRequestWrapper implements Countable, IteratorAggregate
	{
		private $data;

		public static function getInstance()
		{
			static $instance;
			if (!isset($instance)) {
				$instance = new static;
			}
			return $instance;
		}
		protected function __construct($data)
		{
			if (!is_array($data)) {
				throw new HTTPRequestWrapperException('Invalid data. Expected array.');
			}
			$this->data = $data;
		}
		public function export()
		{
			return $this->data;
		}
		public function get($name)
		{
			return $this->data[$name];
		}
		public function isset($name)
		{
			return isset($this->data[$name]);
		}
		public function getNames()
		{
			return array_keys($this->data);
		}
		public function count()
		{
			return count($this->data);
		}
		public function getIterator()
		{
			return new HTTPRequestWrapperIterator($this->data);
		}
	}
	class HTTPRequestWrapperException extends Exception
	{

	}
	final class HTTPRequestWrapperIterator implements Iterator
	{
		private $count;
		private $data;
		private $counter;
	
		public function __construct($data)
		{
			$this->data = $data;
			$this->count = count($data);
			$this->counter = 0;
		}
		public function rewind() 
		{
			$this->counter = 0;
		}
		public function valid()
		{
			return $this->counter < $this->count;
		}
		public function key()
		{
			return array_keys($this->data)[$this->counter];
		}
		public function current()
		{
			return $this->data[array_keys($this->data)[$this->counter]];
		}
		public function next()
		{
			$this->counter++;
		}
	}

	final class HTTPRequestGET extends HTTPRequestWrapper
	{
		protected function __construct()
		{
			parent::__construct($_GET);
		}
	}

	final class HTTPRequestPOST extends HTTPRequestWrapper
	{
		protected function __construct()
		{
			parent::__construct($_POST);
		}
	}
	final class HTTPRequestSERVER extends HTTPRequestWrapper
	{
		protected function __construct()
		{
			parent::__construct($_SERVER);
		}
	}

	final class HTTPResponse 
	{
		private $content;

		public static function getInstance() 
		{
			static $instance;
			if (!isset($instance)) {
				$instance = new self;
			}
			return $instance;
		}
		public function setContent($content) 
		{
			$this->content = $content;
		}
		public function send()
		{
			if (isset($this->content)) {
				echo $this->content;
			}
		}
		private function __construct() 
		{

		}
	}

	class PHPTemplate
	{
		private static $template;
		private static $input;
		public static function view($template, $input = [])
		{
			self::$template = $template;
			self::$input 	= $input;

			unset($template, $input);
			ob_start();	
			try {
				extract(self::$input);
				require(self::$template);
			} catch (Exception $e) {
				ob_end_clean();
				throw new PHP_Template_Exception;
			}
			return ob_get_clean();
		}
	}

	class PHPTemplateException extends Exception
	{

	}

	abstract class AV
	{
		private $httpRequest;
		private $productsModel;
		private $productManager;

		protected function getHTTPRequest() 
		{
			return $this->httpRequest;
		}
		protected function getProductsModel() 
		{
			return $this->productsModel;
		}
		protected function getProductManager()
		{
			return $this->productManager;
		}
		protected function getPOST() 
		{
			return $this->getHttpRequest()->getPOST();
		}
		protected function getGET()
		{
			return $this->getHttpRequest()->getGET();
		}
		protected function getSERVER()
		{
			return $this->getHttpRequest()->getSERVER();
		}
		final public function __construct() {
			$this->httpRequest 		= HTTPRequest::getInstance();
			$this->productsModel 	= ProductsModel::getInstance();
			$this->productManager 	= ProductManager::getInstance();
		}
	}

	abstract class Action extends AV
	{
		abstract public function run();
	}

	abstract class View extends AV
	{
		abstract public function render();
	}

	final class DefaultView extends View 
	{
		public function render() 
		{
			echo 'We are in default view';
			return PHPTemplate::view(
				'templates/default.php'
			);
		}
	}

	final class ShowProductsView extends View 
	{
		public function render() 
		{
			return PHPTemplate::view(
				'templates/show_products.php', 
				$this->getProductsModel()->getProducts()
			);
		}
	}

	final class AddProductView extends View 
	{
		public function render() 
		{
			return PHPTemplate::view(
				'templates/add_product.php'
			);
		}
	}

	final class AddProductAction extends Action 
	{
		public function run() 
		{

			try {
				$this->validate();
				$this->saveProduct();
			} catch (ProductValidationException $e) {
				throw new AddProductActionException($e->getMessage());
			}
		}
		private function validate() {
			$this->validateType();
		}
		private function validateType()
		{
			if (!$this->getPOST()->isset('type')) {
				throw new ProductValidationException('POST request do not have type parameter');
			}

			$types = [
				'dvd',
				'furniture',
				'book',
			];

			if (!in_array($this->getPOST()->get('type'), $types)) {
				throw new ProductValidationException('Type is not valid');
			}
		}
		private function saveProduct()
		{
			$this->getProductsModel()->addProduct($this->getProduct());
		}
		private function getProduct() 
		{
			return $this->getProductManager()->getProduct($this->getPOST()->export());
		}
	}
	class AddProductActionException extends Exception
	{

	}
	class ProductValidationException extends Exception 
	{

	}

	final class DeleteProductsAction extends Action 
	{
		public function run() 
		{
			echo 'We are in delete products action';
			$this->getProductsModel()->deleteProductsByIDs($this->getPOST()->get('md'));
		}
	}

	final class DatabaseManager {
	    
	    private static $db;
	    
	    public static function getDB() 
	    {
	        if (!isset(self::$db)) {
	            self::dbConnect();
	        }
	        return self::$db;
	    }
	    
	    private function __construct()
	    {

	    }
	    
	    public static function dbConnect() 
	    {
	        $db = mysqli_connect('localhost', 'monteby_scandiweb', 'scandiweb_scandiweb', 'monteby_scandiweb');
	        
	        if (!$db) {
	            throw new DatabaseManagerException;
	        }
	        
	        mysqli_query($db, "SET NAMES utf8");
	        mysqli_query($db, "SET CHARACTER_SET_RESULTS='utf8_general_ci'");
	        
	        self::$db = $db;
	    }
	}
	class DatabaseManagerException extends Exception
	{

	}

	class StringHelper {

	}

	final class ProductsModel 
	{
		private $db;
		private function getDB()
		{
			return $this->db;
		}
		public static function getInstance() 
		{
			static $instance;
			if (!isset($instance)) {
				$instance = new self;
			}
			return $instance;
		}
		private function __construct() 
		{
			$this->db = DatabaseManager::getDB();
		}
		public function getProducts() 
		{
	        $query = "SELECT * FROM `products` ORDER BY `sku` ASC;";
	        $products = mysqli_query($this->getDB(), $query);

			$p = [];
	        

	        while ($r = $products->fetch_assoc()) {
	        	$p[] = $r;
	        }

		    return [
	            'count' 	=> mysqli_num_rows($products),
	            'items' 	=> $p,
	        ];
		}
		private function productToArray(Product $product)
		{
			
			$keys = ['sku', 'name', 'price', 'type', 'weight', 'size', 'length', 'width', 'height'];

			$r 					= [];

			$r['sku'] 			= $product->getSKU();
			$r['name'] 			= $product->getName();
			$r['price'] 		= $product->getPrice();
			
			$r['type'] 			= strtolower(get_class($product));

			if (method_exists($product, 'getWeight')) {
				$r['weight'] 	= $product->getWeight();	
			}
			if (method_exists($product, 'getSize')) {
				$r['size'] 	= $product->getSize();	
			}
			if (method_exists($product, 'getLength')) {
				$r['length'] 	= $product->getLength();	
			}
			if (method_exists($product, 'getWidth')) {
				$r['width'] 	= $product->getWidth();	
			}
			if (method_exists($product, 'getHeight')) {
				$r['height'] 	= $product->getHeight();	
			}

			foreach ($keys as $k) {
				if (!array_key_exists($k, $r)) {
					$r[$k] = 'NULL';
				}
			}
			return $r;
		}
		public function addProduct(Product $product) 
		{
			$p = $this->productToArray($product);

			$query = "
            	INSERT INTO `products` 
            	(
            		`sku`, 
            		`name`, 
            		`price`, 
            		`type`, 
            		`size`, 
            		`height`, 
            		`width`, 
            		`length`, 
            		`weight`
            	) 
            	VALUES 
            	(
            		'{$p["sku"]}', 
            		'{$p["name"]}', 
            		{$p["price"]}, 
            		'{$p["type"]}', 
            		{$p["size"]}, 
            		{$p["height"]}, 
            		{$p["width"]}, 
            		{$p["length"]}, 
            		{$p["weight"]}
            	);
            ";

            mysqli_query($this->getDB(), $query);

		}
		public function deleteProductsByIDs($productIDs)
		{
			$query = "DELETE FROM `products` WHERE `sku` IN (" . $this->formatSKUs($productIDs) . ");"; 
        	mysqli_query($this->getDB(), $query);
		}	    
		private function formatSKUs($a) 
		{
	        foreach ($a as &$v) {
	            $v = "'{$v}'";
	        }
	        return implode(',', $a);
	    }
	}

	class ProductsModelException extends Exception 
	{

	}

	class ProductManager {
		private $productValidator;
		public static function getInstance() {
			static $instance;
			if (!isset($instance)) {
				$instance = new self;
			}
			return $instance;
		}
		private function __construct()
		{
			$this->productValidator = ProductValidator::getInstance();
		}
		public function getProduct($input) {
			try {
				$product = new $input['type']($input);
				$this->productValidator->validate($product);
				return $product;
			} catch (Exception $e) {
				throw new ProductManagerException;
			}
		}
	}
	class ProductManagerException extends Exception
	{

	}
	final class ProductValidator {
		public static function getInstance()
		{
			static $instance;
			if (!isset($instance)) {
				$instance = new self;
			}
			return $instance;
		}
		private function __construct()
		{

		}
		public function validate(Product $product)
		{
			if (!$this->validateSKU($product->getSKU())) {
				throw new ProductValidationException;
			}
			if (!$this->validateName($product->getName())) {
				throw new ProductValidationException;
			}
			if (!$this->validatePrice($product->getPrice())) {
				throw new ProductValidationException;
			}
			if (!$this->validateType($product->getType())) {
				throw new ProductValidationException;
			}
			if (method_exists($product, 'getSize')) {
				if (!$this->validateSize($product->getSize())) {
					throw new ProductValidationException;
				}
			}
			if (method_exists($product, 'getHeight')) {
				if (!$this->validateHeight($product->getHeight())) {
					throw new ProductValidationException;
				}
			}
			if (method_exists($product, 'getLength')) {
				if (!$this->validateSize($product->getLength())) {
					throw new ProductValidationException;
				}
			}
			if (method_exists($product, 'getWidth')) {
				if (!$this->validateWidth($product->getWidth())) {
					throw new ProductValidationException;
				}
			}
			if (method_exists($product, 'getWeight')) {
				if (!$this->validateWeight($product->getWeight())) {
					throw new ProductValidationException;
				}
			}
		}
	    private function validateSKU($string) {
	        if (strlen($string) < 3) return false;
	        if (strlen($string) > 20) return false;
        
	        return true;
	    }
	    private function validateName($string) {
	        if (strlen($string) < 3) return false;
	        if (strlen($string) > 50) return false;

	        
	        return true;
	    }
	    private function validatePrice($string) {
	        if (strlen($string) === 0) return false;
	        if (strlen($string) > 8) return false;

	        return true;
	    }
	    private function validateType($string) {
	        if (!in_array($string, array('dvd', 'furniture', 'book'))) return false;

	        return true;
	    }
	    private function validateSize($string) {
	        if (strlen($string) === 0) return false;
	        if (strlen($string) < 2) return false;
	        if (strlen($string) > 5) return false;

	        return true;
	    }
	    private function validateHeight($string) {
	        if (strlen($string) === 0) return false;
	        if (strlen($string) > 4) return false;

	        return true;
	    }
	    private function validateLength($string) {
	        if (strlen($string) === 0) return false;
	        if (strlen($string) > 4) return false;

	        return true;
	    }
	    private function validateWidth($string) {
	        if (strlen($string) === 0) return false;
	        if (strlen($string) > 4) return false;
  
	        return true;
	    }
	    private function validateWeight($string) {
	        if (strlen($string) === 0) return false;
	        if (strlen($string) > 6) return false;

	        return true;
	    }
	}
	abstract class Product 
	{
		private $sku;
		private $name;
		private $price;
		private $type;

		protected function setSKU($sku) 
		{
			$this->sku = $sku;
		}
 		public function getSKU() 
 		{
			return $this->sku;
		}
		protected function setName($name) 
		{
			$this->name = $name;
		}
		public function getName() 
		{
			return $this->name;
		}
		protected function setPrice($price) 
		{
			$this->price = $price;
		}
		public function getPrice() 
		{
			return $this->price;
		}
		protected function setType($type)
		{
			$this->type = $type;
		}
		public function getType()
		{
			return $this->type;
		}
		protected function __construct($input) {
			if (array_key_exists('sku', $input)) {
				$this->setSKU($input['sku']);
			}
			if (array_key_exists('name', $input)) {
				$this->setName($input['name']);
			}
			if (array_key_exists('price', $input)) {
				$this->setPrice($input['price']);
			}
			if (array_key_exists('type', $input)) {
				$this->setType($input['type']);
			}
		}
	}

	class Furniture extends Product 
	{
		private $width;
		private $length;
		private $height;

		public function __construct($input) {
			parent::__construct($input);
			if (array_key_exists('width', $input)) {
				$this->setWidth($input['width']);
			}
			if (array_key_exists('length', $input)) {
				$this->setLenght($input['length']);
			}
			if (array_key_exists('height', $input)) {
				$this->setHeight($input['height']);
			}
		}
		public function setWidth($width) 
		{
			$this->width = $width;
		}
		public function getWidth() 
		{
			return $this->width;
		}
		public function setLenght($length) 
		{
			$this->length = $length;
		}
		public function getLength() 
		{
			return $this->length;
		}
		public function setHeight($height) 
		{
			$this->height = $height;
		}
		public function getHeight() 
		{
			return $this->height;
		}
	}

	class DVD extends Product 
	{
		private $size;

		public function __construct($input) 
		{
			parent::__construct($input);
			if (array_key_exists('size', $input)) {
				$this->setSize($input['size']);
			}
		}
		public function setSize($size) 
		{
			$this->size = $size;
		}
		public function getSize() 
		{
			return $this->size;
		}
	}

	class Book extends Product 
	{
		private $weight;

		public function __construct($input) 
		{
			parent::__construct($input);
			if (array_key_exists('weight', $input)) {
				$this->setWeight($input['weight']);
			}
		}
		public function setWeight($weight) 
		{
			$this->weight = $weight;
		}
		public function getWeight() 
		{
			return $this->weight;
		}
	}

	ini_set('display_errors'			, 	1 		);
	ini_set('display_startup_errors'	, 	1 		);
	ini_set('error_reporting'			, 	E_ALL 	);

	try {	

		FrontController::go();
	
	} catch (Exception $e) {
		
		echo 'Something went wrong';
		Tracer::trace($e->getMessage());
	
	}