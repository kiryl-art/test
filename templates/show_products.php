<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="main.css">
    <title>Product List</title>
</head>
<body>
    <div class="container">
        <form action="/" method="post">
            <div class="header">
                <div class="logo">
                    <h1>Product List</h1>
                </div>
            </div>
            <div class="buttons">
                <a href="addproduct"><button type="button">ADD</button></a>
                <button id="delete-product-btn" type="submit">MASS DELETE</button>
            </div>
            <hr>
            <?php if (!$count): ?>
                <div class="no-products">
                    <p>Products not found!</p>
                    <p class="message">Click "ADD" button to add new product</p>
                </div>
                <?php else: ?>
                    <div class="wrapper">
                        <?php foreach ($items as $item): ?>
                            <div class="card">
                                <input type="checkbox" class="delete-checkbox" value="<?= $item['sku'] ?>" name="md[]">
                                <ul>
                                    <li><?= $item['sku'] ?></li>
                                    <li><?= $item['name'] ?></li>
                                    <li><?= $item['price'] ?> $</li>
                                <?php if ($item['type'] == 'dvd'): ?> 
                                    <li>Size: <?= $item['size'] ?> MB</li>
                                <?php elseif ($item['type'] == 'furniture'): ?>
                                    <li>Dimensions: H<?= $item['height'] ?> x W<?= $item['width'] ?> x L<?= $item['length'] ?></li>
                                <?php elseif ($item['type'] == 'book'): ?>
                                    <li>Weight: <?= $item['weight'] ?> KG</li>
                                <?php endif; ?>
                                </ul>
                            </div>
                        <?php endforeach; ?>
                    </div>       
            <?php endif; ?>
            <hr>
        </form>
        <footer>
            <p>Scandiweb test assignment</p>
        </footer>
    </div>
</body>
</html>