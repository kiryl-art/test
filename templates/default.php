<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="main.css">
    <title>Product list</title>
</head>
<body>
    <div class="container">
        <h1>404 &mdash; Page is not found</h1>
        <footer>
            <p>Scandiweb test assignment</p>
        </footer>
    </div>
</body>
</html>