<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="main.css">
    <title>Product Add</title>
</head>
<body>
    <div class="container">
        <form id="product_form" action="/" method="post">
        <div class="header">
            <div class="logo">
                <h1>Product Add</h1>
            </div>
        </div>
        <div class="buttons">
            <button type="submit">Save</button>
            <button type="reset"><a href="/">Cancel</a></button>
        </div>
        <hr>
            <label for="sku">SKU</label>
            <input id="sku" type="text" name="sku" <?php if (isset($sku)) echo "value=\"{$sku}\"";?>>
            <span id="skuMessage"><?php if (isset($skuMessage)) echo $skuMessage; ?></span><br>
            <label for="name">Name</label>
            <input id="name" type="text" name="name" <?php if (isset($name)) echo "value=\"{$name}\"";?>>
            <span id="nameMessage"><?php if (isset($nameMessage)) echo $nameMessage; ?></span><br>
            <label for="price">Price ($)</label>
            <input id="price" type="number" name="price" <?php if (isset($price)) echo "value=\"{$price}\"";?>>
            <span id="priceMessage"><?php if (isset($priceMessage)) echo $priceMessage; ?></span><br>
            <label class="type_switcher" for="productType">Type Switcher</label>
            <select id="productType" name="type">
            <option value="type_switcher">Type Switcher</option>
                <option value="dvd" <?php if (isset($typeDVD)) echo 'selected'?>>DVD</option>
                <option value="furniture" <?php if (isset($typeFurniture)) echo 'selected'?>>Furniture</option>
                <option value="book" <?php if (isset($typeBook)) echo 'selected'?>>Book</option>
            </select>
            <span id="typeMessage"><?php if (isset($typeMessage)) echo $typeMessage; ?></span><br>
            <div class="dvd op">
                <label for="size">Size (MB)</label>
                <input id="size" name="size" type="number" <?php if (isset($size)) echo "value=\"{$size}\"";?>>
                <span id="sizeMessage"><?php if (isset($sizeMessage)) echo $sizeMessage; ?></span><br>
                <p>"Please, provide size"</p>
            </div>
            <div class="furniture op">
                <label for="height">Height (CM)</label>
                <input id="height" name="height" type="number" <?php if (isset($height)) echo "value=\"{$height}\"";?>>
                <span id="heightMessage"><?php if (isset($heightMessage)) echo $heightMessage; ?></span><br>
                <label for="width">Width (CM)</label>
                <input id="width" name="width" type="number" <?php if (isset($width)) echo "value=\"{$width}\"";?>>
                <span id="widthMessage"><?php if (isset($widthMessage)) echo $widthMessage; ?></span><br>
                <label for="length">Length (CM)</label>
                <input id="length" name="length" type="number" <?php if (isset($length)) echo "value=\"{$length}\"";?>>
                <span id="lengthMesage"><?php if (isset($lengthMessage)) echo $lengthMessage; ?></span><br>
                <p>"Please provide dimensions"</p>
            </div>
            <div class="book op">
                <label for="book">Weight (KG)</label>
                <input id="weight" name="weight" type="number" <?php if (isset($weight)) echo "value=\"{$weight}\"";?>>
                <span id="weightMessage"><?php if (isset($weightMessage)) echo $weightMessage; ?></span><br>
                <p>"Please, provide weight"</p>
            </div>
        </form>
        <footer>
            <hr>
            <p>Scandiweb test assignment</p>
        </footer>
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>